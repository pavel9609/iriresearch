# IRIresearch

Модуль для исследования модели IRI. Позволяет обращаться к онлайн версии IRI, расположенной на https://ccmc.gsfc.nasa.gov/modelweb/models/iri2016_vitmo.php и получать данные с этого сайта. Также данная библиотека позволяет локальной версии модели iri2016 и получать ее данные.

Для запуска нужен python 3 версии не ниже 3.6, а также следующие библиотеки:
* numpy
* pandas
* BeautifullSoup4
* requests
* io
