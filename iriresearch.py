"""Модуль функций для исследования IRI"""


import io
import os
import subprocess
from datetime import datetime  # Для работы со временем

import numpy as np
import pandas as pd
import requests  # Для запросов
from bs4 import BeautifulSoup  # Парсер


def run_online(date_time=datetime.now(), time_flag=0, jmag=0, lat=0, lon=0,
               height=1000, profile=1, start_profile=100, end_profile=2000,
               step_profile=20, ne_top=0, imap=0, ffof2=0, hhmf2=0, ib0=2,
               probab=0, fauroralb=0, ffoe=0, dreg=0, tset=0, icomp=0, tec=2000,
               nmf2=0, hmf2=0, nme=0, hme=0, b0_user=0, sun_n='', ion_n='',
               radio_f='', radio_f81='', parameters=None, debug=False):
    """ Вызывает онлайн версию IRI и возвращает результаты расчета в виде текста ответа
        date_time - время в виде объекта datetime, по умолчанию текущий момент
        time_flag - флаг времени - 0 глобальное время, 1 - локальное время (в зависимости от широты)
        jmag - флаг геомагнитных координат - 0 географические , 1 - геомагнитные
        lat - широта
        lon - долгота
        height - высота
        profile - тип профиля
        start_profile - стартовое значение профиля
        end_profile - конец профиля
        step_profile - шаг
        ffof2: использовать модель штормов F2 или нет
        ne_top - модель ne topside 0 - Nequick, 1 - IRI2001-corr, 2 - IRI2001
        imap - CCIR или URSI
        ib0 - Модель, для вычисления Bottomside Thickness
        probab - модель для вычисления вероятности появления F1-слоя
        fauroralb - Учитывать Auroral boundaries
        ffoe - Использовать E-peak auroral storm model
        dreg - какую модель для D-региона использовать
        tset - Te Topside 0 - TBT2012+SA, 1 - TBT2012, 2 - BIL1995
        icomp - Ion Composition 0 - RBV10/TBT15, 1 - DS95/DY85
        tec - верхняя граница для полного электронного содержания
        nmf2 - значение пиковой концентрации F2 (если требуется ввести свою)
        hmf2 - значение выстоы пиковой концентрации F2 (если требуется ввести свою)
        nme - аналогично nmf2 для слоя e
        hme - аналогичено для hmF2 для слоя e
        user_B0 - значение параметра B0
        sun_n - значение индекса RZ12 (если вводится пользователем)
        ion_n - значение ионосферного индекса IG12 (если вводится пользователем)
        radio_f - значение индекса F10.7 (дневное)
        radio_f81 - значение индекса f10.7 (среднее за 81 день)
        debug - вывод тестовых сообщений
        Подробнее о значениях индексов и о параметрах можно прочитать на  https://ccmc.gsfc.nasa.gov/modelweb/models/iri2016_help.html
    """
    if jmag == 0:
        jmag = '0.'
    else:
        jmag = '1.'
    hour = date_time.hour + date_time.minute / 60
    if parameters is None:
        parameters = list(range(54))
    try:
        response = requests.post('https://ccmc.gsfc.nasa.gov/cgi-bin/modelweb/models/vitmo_model.cgi',
                                 data={'model': 'iri2016',
                                       'year': date_time.year,
                                       'month': date_time.month,
                                       'day': date_time.day,
                                       'time_flag': time_flag,
                                       'hour': hour, 'geo_flag': jmag,
                                       'latitude': lat, 'longitude': lon,
                                       'height': height, 'profile': profile,
                                       'start': start_profile,
                                       'stop': end_profile,
                                       'step': step_profile, 'sun_n': sun_n,
                                       'ion_n': ion_n, 'radio_f': radio_f,
                                       'radio_f81': radio_f81, 'htec_max': tec,
                                       'ne_top': ne_top, 'imap': imap,
                                       'ffof2': ffof2, 'hhmf2': hhmf2,
                                       'ib0': ib0, 'probab': probab,
                                       'fauroralb': fauroralb, 'ffoE': ffoe,
                                       'dreg': dreg, 'tset': tset,
                                       'icomp': icomp, 'nmf2': nmf2,
                                       'hmf2': hmf2, 'nme': nme,
                                       'user_hme': hme,
                                       'user_B0': b0_user, 'format': 2,
                                       'vars': parameters, 'linestyle': 'solid',
                                       'charsize': '', 'symbol': 2,
                                       'symsize': '', 'yscale': 'Linear',
                                       'xscale': 'Linear', 'imagex': 640,
                                       'imagey': 480},
                                 headers={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36'})
        soup = BeautifulSoup(response.text, 'html.parser') #Парсим ответ
        for link in soup.find_all('a'): #Ищем гиперссылку
            href = link.get('href') #Находим
            if href.find('.lst') != -1: #Проверяем на то, что это нужная нам
                lst_response = requests.post(href)
                if debug:
                    print(lst_response)
                return lst_response.text
    except requests.exceptions.Timeout:
        print('Нет ответа от ccmc')
        return None
    except requests.exceptions.RequestException as exp:
        print(exp)
        return None
    return None

def create_df(data, source='response', names=None, prefix=None):
    """Создает dataframe для pandas из данных полученных из локальной или онлайн версии
        data - источник данных
        type - тип данных
            response - берет ответ, полученный с сайта и получает из него dataframe с именами столбцов, указанных в names (если None, то используется стандартный набор)
            fbinary - в data подается многомерный массив numpy (numpy.ndarray) и на основе его типов создается dataframe
        names - названия столбцов
        prefix - добавления к имени столбцов
    """
    if source not in ['response', 'fbinary']:
        print('Wrong source type')
        return None
    if names is None: #Имена колонок по умолчанию, не стал выносить в параметры, так как много места занимают
        names = ['year', 'month', 'day of month', 'day of year', 'hour',
                 'solar zenith angle', 'height', 'lat', 'lon', 'CGM lat',
                 'CGM lon', 'DIP', 'Modified DIP', 'Declination', 'InvDip',
                 'DIP lat', 'MLT', 'Ne', 'Ne/NmF2', 'Tn', 'Ti', 'Te', 'O+',
                 'H+', 'He+', 'O2+', 'NO+', 'cluster ions', 'N+', 'TEC',
                 'TEC %', 'hmF2', 'hmF1', 'hmE', 'hmD', 'NmF2', 'NmF1',
                 'NmE', 'NmD', 'M3000F2', 'B0', 'B1', 'E-walley width',
                 'E-walley depth', 'foF2', 'foF1', 'foE', 'foD',
                 'Equatorial vertical ion drift', 'Ratio of foF2 storm to foF2',
                 'F1 probability', 'CGM lat of auroral oval boundary',
                 'Ratio foE storm to foE quiet',
                 'Spread-F probability']
    if prefix is not None: #Добавляем префикс
        names = [prefix + name for name in names]
    if source == 'response':
        return pd.read_csv(io.StringIO(data), header=None,
                           sep=r'\s+', names=names)
    if source == 'fbinary':
        return pd.DataFrame(data=data, columns=data.dtype.names)
    return None



def run_local(date_time=datetime.now(), jmag=0, lat=-90, lon=120,
              time_flag=0, height=1000, tec=2000, profile=8, start_profile=0.00,
              end_profile=23.99, step_profile=0.25, ne_top=0, imap=0, ffof2=0,
              hhmf2=0, ib0=2, probab=0, fauroralb=0, ffoe=0, dreg=0, tset=0,
              icomp=0, sun_n='', ion_n='', radio_f='', radio_f81='',
              iri_dir='iri2016', iri_exe='iri2016.exe', datatype=None,
              debug=False):
    """Запускает локальную версию, которая должна находится в папке iri2016
        Параметры аналогичны run_online
        iri_dir - директория, где находится модель iri
        iri_exe - исполняемый файл iri
        datatype - тип для обработки бинарных файлов фортрана
    """
    hour = date_time.hour + date_time.minute / 60
    mmdd = date_time.month * 100 + date_time.day
    os.chdir(iri_dir)
    if sun_n == '':
        sun_n = -1
    if ion_n == '':
        ion_n = -1
    if radio_f == '':
        radio_f = -1
    if radio_f81 == '':
        radio_f81 = -1
    #Отправляем данные в stdin модели непосредственно с python
    process = subprocess.Popen([iri_exe], stdin=subprocess.PIPE)
    #Клеим строку, которую скормим программе на фортранее
    param_string = make_input_string([jmag, lat, lon, date_time.year, mmdd,
                                      time_flag, hour, height, tec, profile,
                                      start_profile, end_profile, step_profile,
                                      ne_top, ffof2, ffoe, fauroralb, hhmf2,
                                      ib0, probab, imap, dreg, tset, icomp,
                                      sun_n, ion_n, radio_f, radio_f81])
    if debug:
        print(param_string)
    process.communicate(input=param_string.encode())
    fbin = open('iri2016.bin', 'rb')
    process.terminate()  # На всякий случай
    os.chdir('..')  #Возвращаемся в исходную папку
    # Указываем типы столбцов
    if datatype is None:
        datatype = np.dtype([('year', '<i4'), ('month', '<i4'),
                             ('day of month', '<i4'),
                             ('day of year', '<f4'), ('hour', np.float32),
                             ('solar zenith angle', np.float32),
                             ('height', np.float32), ('lat', np.float32),
                             ('lon', np.float32), ('GCM lat', np.float32),
                             ('GCM lon', np.float32), ('DIP', np.float32),
                             ('Modified DIP', np.float32),
                             ('Declination', np.float32),
                             ('INVDIP', np.float32), ('DIP lat', np.float32),
                             ('MLT', np.float32), ('Ne', np.float32),
                             ('Ne/NmF2', np.float32), ('Tn', np.float32),
                             ('Ti', np.float32), ('Te', np.float32),
                             ('O+', np.float32), ('H+', np.float32),
                             ('He+', np.float32), ('O2+', np.float32),
                             ('NO+', np.float32), ('cluster ions', np.float32),
                             ('N+', np.float32), ('TEC', np.float32),
                             ('TEC %', np.float32), ('hmF2', np.float32),
                             ('hmF1', np.float32), ('hmE', np.float32),
                             ('hmD', np.float32), ('NmF2', np.float32),
                             ('NmF1', np.float32), ('NmE', np.float32),
                             ('NmD', np.float32), ('M3000F2', np.float32),
                             ('B0', np.float32), ('B1', np.float32),
                             ('E-walley width', np.float32),
                             ('E-walley depth', np.float32),
                             ('foF2', np.float32), ('foF1', np.float32),
                             ('ffoE', np.float32), ('ffoD', np.float32),
                             ('Equatorial vertical ion drift', np.float32),
                             ('Ratio of foF2 storm to foF2', np.float32),
                             ('F1 probabilty', np.float32),
                             ('CGM lat of auroral oval boundary', np.float32),
                             ('Ratio foE storm to foE quiet', np.float32),
                             ('Spread-F probability', np.float32)])
    iri_data = np.fromfile(fbin, dtype=datatype)
    return iri_data

def make_input_string(params):
    """Создает строку из массива параметров через запятую и подает ее"""
    param_string = ''
    for param in params:
        param_string = param_string + str(param)
        param_string = param_string + ','
    param_string = param_string[:-1]  #Обрезаем последнюю запятую
    return param_string
